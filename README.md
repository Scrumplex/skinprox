<!--
SPDX-FileCopyrightText: 2022 Sefa Eyeoglu <contact@scrumplex.net>

SPDX-License-Identifier: CC0-1.0
-->

## skinprox

Opinionated resolver DDraceNetwork skins

This software resolves skins for DDraceNetwork from multiple skin servers.

# Build & Run

```shell
$ cargo run
```

# Usage

```
Combine multiple DDraceNetwork skin services into one

Usage: skinprox [OPTIONS] --providers <PROVIDERS>

Options:
  -l, --listen-address <LISTEN_ADDRESS>  [env: LISTEN_ADDRESS=] [default: 0.0.0.0:3000]
  -u, --public-url <PUBLIC_URL>          [env: PUBLIC_URL=] [default: http://skinprox.invalid]
  -t, --timeout <TIMEOUT>                [env: TIMEOUT=] [default: 2]
  -p, --providers <PROVIDERS>            [env: PROVIDERS=]
  -h, --help                             Print help
  -V, --version                          Print version
```

If you need to specify multiple values in an env var, the seperator is a comma (`,`).
For example:

```
SKINPROX_PROVIDERS="https://skins.ddnet.org/skin/community/,https://api.skins.tw/api/resolve/skins/"
```

# License

This software is licensed under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE.
You can read the license text in [LICENSE](./LICENSE).
Additionally, this software is REUSE compliant!
