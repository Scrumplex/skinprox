# SPDX-FileCopyrightText: 2024 Sefa Eyeoglu <contact@scrumplex.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  lib,
  self,
  ...
}: {
  perSystem = {pkgs, ...}: {
    checks.reuse = pkgs.runCommandNoCC "reuse-lint" {} ''
      cd ${self}
      exec ${lib.getExe pkgs.reuse} lint | tee "$out"
    '';
  };
}
