# SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  lib,
  rustPlatform,
  pkg-config,
  openssl,
  # flake
  version,
  src,
}:
rustPlatform.buildRustPackage {
  pname = "skinprox";
  inherit version src;

  nativeBuildInputs = [pkg-config];
  buildInputs = [openssl];

  vendorHash = null;
  cargoLock.lockFile = ../../Cargo.lock;

  meta = with lib; {
    homepage = "https://codeberg.org/Scrumplex/skinprox";
    description = "Opinionated proxy for querying skins for DDraceNetwork";
    longDescription = ''
      This software resolves skins for DDraceNetwork from multiple skin servers.
    '';
    platforms = platforms.unix;
    license = licenses.agpl3Plus;
    maintainers = with maintainers; [Scrumplex];
    mainProgram = "skinprox";
  };
}
