# SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later
{module, ...}: {
  name = "skinprox-test";

  nodes.machine = {pkgs, ...}: {
    imports = [
      module
    ];

    environment.systemPackages = with pkgs; [curl];

    services.static-web-server = {
      enable = true;
      root = pkgs.runCommandNoCCLocal "static-files" {} ''
        mkdir $out
        echo "foobar" > $out/example.txt
      '';
    };

    services.skinprox = {
      enable = true;
      listenHost = "127.0.0.1";
      listenPort = 3001;
      publicUrl = "http://localhost:3001";
      providers = [
        "https://skins.ddnet.org/skin/community/"
        "https://skins.tee.world/"
        "https://api.skins.tw/api/resolve/skins/"
      ];
    };
  };

  testScript = ''
    start_all()
    machine.wait_for_unit("skinprox.service")
    machine.succeed("curl -sSv http://localhost:3001")
    machine.succeed("curl -sSv http://localhost:3001/skin/example.txt")
  '';
}
