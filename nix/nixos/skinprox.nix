# SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  config,
  lib,
  pkgs,
  ...
}: let
  inherit (builtins) concatStringsSep;
  inherit (lib.meta) getExe;
  inherit (lib.modules) mkIf;
  inherit (lib.options) mkEnableOption mkOption mkPackageOption;
  inherit (lib) types;

  cfg = config.services.skinprox;

  listenStream =
    if cfg.listenHost == null
    then toString cfg.listenPort
    else "${cfg.listenHost}:${toString cfg.listenPort}";
in {
  options.services.skinprox = {
    enable = mkEnableOption "skinprox service";

    package = mkPackageOption pkgs "skinprox" {};

    listenHost = mkOption {
      type = with types; nullOr str;
      default = null;
      example = "0.0.0.0";
      description = ''
        Listen address of HTTP service.

        If null, it will listen on all addresses.
      '';
    };

    listenPort = mkOption {
      type = with types; port;
      default = 3000;
      example = 8080;
      description = ''
        Listen port of HTTP service
      '';
    };

    publicUrl = mkOption {
      type = with types; str;
      example = "https://skins.example.com";
      description = ''
        Publicly accessible URL for skinprox. Visible on landing page.
      '';
    };

    providers = mkOption {
      type = with types; nullOr (listOf str);
      default = null;
      example = [
        "https://skins.ddnet.org/skin/community/"
        "https://api.skins.tw/api/resolve/skins/"
      ];
      description = ''
        List of skin providers used by skinprox.
      '';
    };
  };

  config = mkIf cfg.enable {
    systemd = {
      services."skinprox" = {
        description = "skinprox DDNet skin proxy";
        after = ["network-online.target"];
        wants = ["network-online.target"];
        wantedBy = ["multi-user.target"];

        environment = {
          PUBLIC_URL = cfg.publicUrl;
          PROVIDERS = concatStringsSep "," cfg.providers;
        };

        serviceConfig = {
          ExecStart = getExe cfg.package;
          Restart = "on-failure";
          DynamicUser = true;

          # Hardening
          CapabilityBoundingSet = "";
          LockPersonality = true;
          MemoryDenyWriteExecute = true;

          PrivateDevices = true;
          PrivateUsers = true;
          ProcSubset = "pid";
          ProtectClock = true;
          ProtectControlGroups = true;
          ProtectHome = true;
          ProtectHostname = true;
          ProtectKernelLogs = true;
          ProtectKernelModules = true;
          ProtectKernelTunables = true;
          ProtectProc = "invisible";
          RestrictNamespaces = true;
          RestrictRealtime = true;
          SystemCallArchitectures = "native";
          SystemCallFilter = [
            "@system-service"
            "~@resources"
            "~@privileged"
          ];
        };
      };
      sockets."skinprox" = {
        inherit (config.systemd.services."skinprox") description wantedBy;

        socketConfig = {
          ListenStream = listenStream;
        };
      };
    };
  };
}
