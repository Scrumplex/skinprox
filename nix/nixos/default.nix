# SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later
{self, ...}: {
  flake.nixosModules = {
    default = self.nixosModules.skinprox;
    skinprox = {
      imports = [self.nixosModules.skinproxBare];
      nixpkgs.overlays = [self.overlays.default];
    };

    skinproxBare = ./skinprox.nix;
  };

  perSystem = {pkgs, ...}: {
    checks.module-test = pkgs.nixosTest (import ./test.nix {module = self.nixosModules.skinprox;});
  };
}
