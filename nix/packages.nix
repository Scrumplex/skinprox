# SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  inputs,
  self,
  ...
}: let
  version = builtins.substring 0 8 self.lastModifiedDate;

  nix-filter = inputs.nix-filter.lib;
  src = nix-filter {
    root = self;

    include = [
      "src"
      "Cargo.lock"
      "Cargo.toml"
    ];
  };
in {
  perSystem = {
    self',
    pkgs,
    ...
  }: {
    packages = {
      default = self'.packages.skinprox;
      skinprox = pkgs.callPackage ./pkgs/skinprox.nix {inherit version src;};
    };
  };

  flake.overlays.default = final: _: {
    skinprox = final.callPackage ./pkgs/skinprox.nix {inherit version src;};
  };
}
