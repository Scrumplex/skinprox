// SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use axum::{extract, response::Redirect};
use indoc::formatdoc;

use crate::{
    response,
    util::{resolve_skin, SKIN_NAME_REGEX},
    AppState,
};

pub async fn fallback() -> response::Error {
    response::Error::NotFound
}

pub async fn index(extract::State(state): extract::State<AppState>) -> String {
    let providers: Vec<String> = state.opts.providers.iter().map(|x| x.to_string()).collect();
    let providers = providers.join("\n");

    let homepage = option_env!("CARGO_PKG_HOMEPAGE").unwrap_or("unknown");

    formatdoc! {"This is an instance of skinprox, a caching skin resolver proxy for DDNet and other Teeworlds (0.6) mods.
        You can find out more about skinprox on its homepage here: {homepage}

        If you want to use this as your skin provider in game, run the following command in the console:

            cl_skin_download_url \"{public_url}/skin/\"


        This instance queries the following sources (in the specified order):

        {providers}
    ", homepage = homepage, public_url = state.opts.public_url, providers = providers}
}

pub async fn proxy_skin(
    extract::State(state): extract::State<AppState>,
    extract::Path(name): extract::Path<String>,
) -> Result<Redirect, response::Error> {
    // simple sanitization
    let name = SKIN_NAME_REGEX
        .find(&name)
        .ok_or(response::Error::BadSkinName)?
        .as_str();

    let future_skin = resolve_skin(state.client.clone(), &state.opts.providers, name);

    state
        .provider_cache
        .get_with(name.to_string(), future_skin)
        .await
        .map(|x| Redirect::to(&x))
}
