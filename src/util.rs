// SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use log::{debug, error};
use once_cell::sync::Lazy;
use regex::Regex;
use reqwest::Client;
use url::Url;

use crate::response;

pub static SKIN_NAME_REGEX: Lazy<Regex> = Lazy::new(|| Regex::new(r"[\w -]+\.\w+$").unwrap());

pub async fn query_provider(
    client: &Client,
    provider: &Url,
    skin_name: &str,
) -> Result<String, response::Error> {
    let request_url = provider.join(skin_name).unwrap().to_string();

    debug!("Resolving skin {} at {}", skin_name, request_url);

    client
        .head(&request_url)
        .send()
        .await
        .map_err(|e| {
            if e.is_timeout() {
                response::Error::TimedOut
            } else {
                error!("Error querying provider: {}", e);
                response::Error::ProviderError
            }
        })
        .and_then(|r| {
            if r.status().is_success() || r.status().is_redirection() {
                debug!("Found skin {} at {}", skin_name, request_url);
                return Ok(request_url);
            }
            debug!("Didn't find skin {} at {}", skin_name, request_url);
            Err(response::Error::SkinNotFound)
        })
}

pub async fn resolve_skin(
    client: Client,
    providers: &Vec<Url>,
    skin_name: &str,
) -> Result<String, response::Error> {
    let mut last_err: Option<response::Error> = None;
    for provider in providers {
        match query_provider(&client, provider, skin_name).await {
            Ok(r) => return Ok(r),
            Err(e) => last_err = Some(e),
        }
    }
    Err(last_err.unwrap_or(response::Error::Unknown))
}
